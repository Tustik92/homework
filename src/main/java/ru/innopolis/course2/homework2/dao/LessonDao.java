package ru.innopolis.course2.homework2.dao;

import ru.innopolis.course2.homework2.entities.Lesson;
import ru.innopolis.course2.homework2.utils.DataBaseException;

import java.util.List;

/**
 * Интерфейс для Dao для работы с уроками, содержит функционал получения урока по идентификатору,
 * добавления, а также получения списка всех уроков и списка уроков учителя.
 *
 * @author Ruslan Tupikov
 */
public interface LessonDao {

    public Lesson read(int key) throws DataBaseException;

    public void save(Lesson lesson) throws DataBaseException;

    public List<Lesson> getAll() throws DataBaseException;

    public List<Lesson> getAll(Integer teacher_id) throws DataBaseException;
}
