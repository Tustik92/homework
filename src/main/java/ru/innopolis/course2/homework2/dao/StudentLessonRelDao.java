package ru.innopolis.course2.homework2.dao;

import ru.innopolis.course2.homework2.entities.Lesson;
import ru.innopolis.course2.homework2.utils.DataBaseException;

import java.util.List;

/**
 * Интерфейс для Dao для работы с посещением уроков учениками, содержит функционал получения
 * списка идентификаторов уроков определенного ученика и списка
 * идентификаторов учеников, записанных на урок.
 *
 * @author Ruslan Tupikov
 */
public interface StudentLessonRelDao {

    public List<Integer> getLessonStudents(Integer lesson_id) throws DataBaseException;

    public List<Integer> getStudentLessons(Integer student_id) throws DataBaseException;
}
