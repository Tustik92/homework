package ru.innopolis.course2.homework2.dao.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.course2.homework2.dao.UserDao;
import ru.innopolis.course2.homework2.entities.User;
import ru.innopolis.course2.homework2.utils.ConnectionPool;
import ru.innopolis.course2.homework2.utils.DataBaseException;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

import static ru.innopolis.course2.homework2.utils.HibernateUtil.getEm;

/**
 * Created by Андрей on 10.12.2016.
 */
@Component
public class HibernateUserDao implements UserDao {

    /**
     * Получение объекта пользователя из БД по его уникальному идентификатору
     * @param key идентификатор пользователяя
     * @return ссылку на пользователя, если пользователь с таким идентификатором найден в БД
     *         null, если в БД нет пользователя с таким идентификатором
     * @throws DataBaseException при ошибке во время работы с БД
     */
    @Override
    public User read(int key) {
        EntityManager manager = getEm();
        manager.getTransaction().begin();
        User user = manager.find(User.class, key);
        manager.getTransaction().commit();
        manager.close();
        return user;
    }

    @Override
    public User read(String login) {
        EntityManager manager = getEm();
        manager.getTransaction().begin();
        TypedQuery<User> tq = manager.createQuery("from User WHERE login= '" + login + "'", User.class);
        User user = tq.getSingleResult();
        manager.getTransaction().commit();
        manager.close();
        return user;
    }

    /**
     * Сохраняет информацию о пользрователе в БД. Если пользователь содержит идентификатор,
     * то обновляет информацию об этом пользователе, если нет, то добавляет пользователя в БД
     * и присваивает еу уникальный идентификатор
     *
     * @param user пользователь, информацию о котором надо сохранить в БД
     * @return информацию о пользователе
     * @throws DataBaseException при возникновении ошибки при сохранении информации о пользователе
     */
    @Override
    public User save(User user) {
        EntityManager manager = getEm();

        manager.getTransaction().begin();
        if (user.getId() == null) {
            manager.persist(user);
        }
        else{
            User old_user = manager.find(User.class, user.getId());
            old_user.setName(user.getName());
            old_user.setBlock(user.getBlock());
            old_user.setLogin(user.getLogin());
            old_user.setPass(user.getPass());
            old_user.setType(user.getType());
        }
        manager.getTransaction().commit();
        manager.close();
        return read(user.getLogin());
    }

    /**
     * Функция удаления информации о пользователе из БД
     *
     * @param user объект с информацией о пользователе, подлежащем удалению
     * @throws DataBaseException при возникновении ошибок при удалении
     */
    @Override
    public void delete(User user) throws DataBaseException {
        EntityManager  manager = getEm();
        manager.getTransaction().begin();
        manager.remove(user);
        manager.getTransaction().commit();
        manager.close();
    }

    /**
     * Функция получения информации обо всех пользователях, зарегистрированных в системе4
     *
     * @return Возвращает список пользователей соответствующих всем записям в базе данных
     * @throws DataBaseException При ошибке во время работы с БД
     */
    @Override
    public List<User> getAll() {
        EntityManager manager = getEm();
        manager.getTransaction().begin();
        Query query = manager.createQuery("from User", User.class);
        List<User> users = query.getResultList();
        manager.getTransaction().commit();
        manager.close();
        return users;
    }

}
