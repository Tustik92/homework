package ru.innopolis.course2.homework2.dao;

import ru.innopolis.course2.homework2.entities.User;
import ru.innopolis.course2.homework2.utils.DataBaseException;


import java.util.List;

/**
 * Интерфейс для Dao для работы с пользователями, содержит функционал получения пользователя по идентификатору,
 * добавления, удаления, редактирования пользователя, а также получения списка всех пользователей.
 *
 * @author Ruslan Tupikov
 */
public interface UserDao {

    public User read(int key) throws DataBaseException;

    public User read(String login) throws DataBaseException;

    public User save(User user) throws DataBaseException;

    public void delete(User user) throws DataBaseException;

    public List<User> getAll() throws DataBaseException;
}
