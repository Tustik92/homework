package ru.innopolis.course2.homework2.dao.mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.course2.homework2.dao.LessonDao;
import ru.innopolis.course2.homework2.entities.Lesson;
import ru.innopolis.course2.homework2.utils.ConnectionPool;
import ru.innopolis.course2.homework2.utils.DataBaseException;

import javax.naming.NamingException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;

import static ru.innopolis.course2.homework2.utils.Constants.*;

/**
 * Интерфейс для Dao для работы с уроками, хранящимися в MySQL базе данных
 * содержит функционал получения урока по идентификатору, добавления, а также
 * получения списка всех уроков и списка уроков учителя.
 *
 * @author Ruslan Tupikov
 */
public class MySQLLessonDao implements LessonDao {

    /**
     * Ссылка на пул соединений для получения соединений с базой данных
     */
    @Autowired
    private ConnectionPool connectionPool;

    /**
     * Получение информации об уроке по его идентификатору
     * @param key идентификатор урока
     * @return объект Lesson, содержащий информацию об уроке
     * @throws DataBaseException привозникновении ошибки при работе с БД илиесли нет урока с таким ID
     */
    @Override
    public Lesson read(int key) throws DataBaseException {
        try (Connection connection = connectionPool.getConnectionPool().getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM lessons WHERE id = '" + key + "'");
            if (resultSet.next()) {
                Timestamp timeStamp = resultSet.getTimestamp("date");
                Lesson lesson = new Lesson()
                        .setId(resultSet.getInt("id"))
                        .setTeacher(resultSet.getInt("teacher"))
                        .setTopic(resultSet.getString("topic"))
                        .setDate(timeStamp)
                        .setMinutes(resultSet.getInt("minutes"));
                return lesson;
            }
            else {
                throw new DataBaseException(LESSON_NOT_FOUND);
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw new DataBaseException(DATABASE_ERROR, e);
        }
    }

    /**
     * Функция для добавления урока в базу данных
     * @param lesson объект урока для сохранения в базу
     * @throws DataBaseException привозникновении ошибки при работе с БД
     */
    @Override
    public void save(Lesson lesson) throws DataBaseException {
        java.util.Date date = lesson.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(date);
        String sql = "INSERT INTO lessons (`teacher`, `topic`, `date`, `minutes`) VALUES ('" + lesson.getTeacher() + "', '" + lesson.getTopic() + "', '" + currentTime + "', '" + lesson.getMinutes() + "')";

        try (Connection connection = connectionPool.getConnectionPool().getConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw new DataBaseException(DATABASE_ERROR, e);
        }
    }

    /**
     * Перегруженный метод getAll(Integer) для получения полного списка уроков
     * @return список всех уроков
     * @throws DataBaseException привозникновении ошибки при работе с БД
     */
    @Override
    public List<Lesson> getAll() throws DataBaseException {
        return getAll(null);
    }

    /**
     * Функция для получения списка уроков по идентификатору учителя,
     * создавшего их. Если идентификатор учителя равен null, то будут возвращены все уроки.
     * @param teacher_id идентификатор учителя или null для получения всех уроков.
     * @return список полученных уроков
     * @throws DataBaseException привозникновении ошибки при работе с БД
     */
    @Override
    public List<Lesson> getAll(Integer teacher_id) throws DataBaseException {
        List<Lesson> lessons = new ArrayList<>();
        String sql = "SELECT * FROM lessons";
        if (teacher_id != null) {
            sql += " WHERE teacher = '" + teacher_id + "'";
        }
        sql += " ORDER BY `date` ASC";
        try (Connection connection = connectionPool.getConnectionPool().getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Timestamp timeStamp =  rs.getTimestamp("date");
                Lesson lesson = new Lesson()
                        .setId(rs.getInt("id"))
                        .setTeacher(rs.getInt("teacher"))
                        .setTopic(rs.getString("topic"))
                        .setDate(timeStamp)
                        .setMinutes(rs.getInt("minutes"));
                lessons.add(lesson);
            }
            return lessons;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw new DataBaseException(DATABASE_ERROR, e);
        }
    }
}
