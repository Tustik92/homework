package ru.innopolis.course2.homework2.dao.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.course2.homework2.dao.LessonDao;
import ru.innopolis.course2.homework2.entities.Lesson;
import ru.innopolis.course2.homework2.utils.ConnectionPool;
import ru.innopolis.course2.homework2.utils.DataBaseException;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static ru.innopolis.course2.homework2.utils.Constants.DATABASE_ERROR;
import static ru.innopolis.course2.homework2.utils.Constants.LESSON_NOT_FOUND;
import static ru.innopolis.course2.homework2.utils.HibernateUtil.getEm;

/**
 * Интерфейс для Dao для работы с уроками, хранящимися в MySQL базе данных
 * содержит функционал получения урока по идентификатору, добавления, а также
 * получения списка всех уроков и списка уроков учителя.
 *
 * @author Ruslan Tupikov
 */
@Component
public class HibernateLessonDao implements LessonDao {

    /**
     * Получение информации об уроке по его идентификатору
     * @param key идентификатор урока
     * @return объект Lesson, содержащий информацию об уроке
     * @throws DataBaseException привозникновении ошибки при работе с БД илиесли нет урока с таким ID
     */
    @Override
    public Lesson read(int key) {
        EntityManager manager = getEm();
        manager.getTransaction().begin();
        Lesson lesson = manager.find(Lesson.class, key);
        manager.getTransaction().commit();
        manager.close();
        return lesson;
    }

    /**
     * Функция для добавления урока в базу данных
     * @param lesson объект урока для сохранения в базу
     * @throws DataBaseException привозникновении ошибки при работе с БД
     */
    @Override
    public void save(Lesson lesson) throws DataBaseException {

        EntityManager manager = getEm();

        manager.getTransaction().begin();
        manager.persist(lesson);
        manager.getTransaction().commit();
        manager.close();
    }

    /**
     * Перегруженный метод getAll(Integer) для получения полного списка уроков
     * @return список всех уроков
     * @throws DataBaseException привозникновении ошибки при работе с БД
     */
    @Override
    public List<Lesson> getAll() throws DataBaseException {
        return getAll(null);
    }

    /**
     * Функция для получения списка уроков по идентификатору учителя,
     * создавшего их. Если идентификатор учителя равен null, то будут возвращены все уроки.
     * @param teacher_id идентификатор учителя или null для получения всех уроков.
     * @return список полученных уроков
     * @throws DataBaseException привозникновении ошибки при работе с БД
     */
    @Override
    public List<Lesson> getAll(Integer teacher_id) throws DataBaseException {

        EntityManager manager = getEm();
        manager.getTransaction().begin();
        String sql = "from Lesson";
        if (teacher_id != null) {
            sql += " WHERE teacher='" + teacher_id + "'";
        }
        Query query = manager.createQuery(sql, Lesson.class);
        List<Lesson> lessons = query.getResultList();
        manager.getTransaction().commit();
        manager.close();
        return lessons;
    }
}
