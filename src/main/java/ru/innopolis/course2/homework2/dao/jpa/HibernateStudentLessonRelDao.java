package ru.innopolis.course2.homework2.dao.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.course2.homework2.dao.StudentLessonRelDao;
import ru.innopolis.course2.homework2.utils.ConnectionPool;
import ru.innopolis.course2.homework2.utils.DataBaseException;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static ru.innopolis.course2.homework2.utils.Constants.DATABASE_ERROR;
import static ru.innopolis.course2.homework2.utils.HibernateUtil.getEm;

/**
 * Интерфейс для Dao для работы с записью на уроки, хранящимися в MySQL базе данных
 * содержит функционал получения списка идентификаторов пользователей, записанных на урок
 * и списка уроков, на которые записан ученик.
 *
 * @author Ruslan Tupikov
 */
@Component
public class HibernateStudentLessonRelDao implements StudentLessonRelDao {

    /**
     * Получение списка идентификаторов пользователей, записанных на урок
     * @param lessonId идентификатор урока для получения пользователей
     * @return список идентификаторов пользователей
     * @throws DataBaseException привозникновении ошибки при работе с БД
     */
    @Override
    public List<Integer> getLessonStudents(Integer lessonId) throws DataBaseException {
        EntityManager manager = getEm();
        manager.getTransaction().begin();
        Query q = manager.createNativeQuery("SELECT `student_id` FROM `students-lessons-rel` WHERE `lesson_id` = '" + lessonId + "'");
        return q.getResultList();
    }

    /**
     * Получение списка идентификаторов уроков, на которые записан ученик
     * @param student_id идентификатор ученика для получения списка уроков
     * @return список идентификаторов уроков
     * @throws DataBaseException привозникновении ошибки при работе с БД
     */
    @Override
    public List<Integer> getStudentLessons(Integer student_id) throws DataBaseException {
        return null;
    }
}
