package ru.innopolis.course2.homework2.dao.mysql;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.course2.homework2.dao.UserDao;
import ru.innopolis.course2.homework2.entities.User;
import ru.innopolis.course2.homework2.utils.ConnectionPool;
import ru.innopolis.course2.homework2.utils.DataBaseException;

import javax.naming.NamingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ru.innopolis.course2.homework2.utils.Constants.*;

/**
 * Dao для работы с пользователями, хранящимися в MySQL базе данных,
 * содержит функционал получения пользователя по идентификатору, добавления,
 * удаления, редактирования пользователя, а также получения списка всех пользователей.
 *
 * @author Ruslan Tupikov
 */
public class MySQLUserDao implements UserDao {

    /**
     * Ссылка на пул соединений для получения соединений с базой данных
     */
    @Autowired
    private ConnectionPool connectionPool;

    /**
     * Получение объекта пользователя из БД по его уникальному идентификатору
     * @param key идентификатор пользователяя
     * @return ссылку на пользователя, если пользователь с таким идентификатором найден в БД
     *         null, если в БД нет пользователя с таким идентификатором
     * @throws DataBaseException при ошибке во время работы с БД
     */
    @Override
    public User read(int key) throws DataBaseException {
        String sql = "SELECT * FROM users WHERE id = ?;";
        PreparedStatement stm;
        try (Connection connection = connectionPool.getConnectionPool().getConnection()){
            stm = connection.prepareStatement(sql);
            stm.setInt(1, key);
            ResultSet rs = stm.executeQuery();
            if(rs.next()) {
                User user = new User(new User.Builder().withLogin(rs.getString("login"))
                        .withPass(rs.getString("pass")).withName(rs.getString("name"))
                        .withBlock(rs.getInt("block")).withType(rs.getInt("type")).withId(key));
                return user;
            }
            else {
                return null;
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw new DataBaseException(DATABASE_ERROR, e);
        }
    }

    @Override
    public User read(String login) throws DataBaseException {
        String sql = "SELECT * FROM users WHERE login = ?;";
        PreparedStatement stm;
        try (Connection connection = connectionPool.getConnectionPool().getConnection()){
            stm = connection.prepareStatement(sql);
            stm.setString(1, login);
            ResultSet rs = stm.executeQuery();
            if(rs.next()) {
                User user = new User(new User.Builder().withLogin(rs.getString("login"))
                        .withPass(rs.getString("pass")).withName(rs.getString("name"))
                        .withBlock(rs.getInt("block")).withType(rs.getInt("type")).withId(rs.getInt("id")));
                return user;
            }
            else {
                return null;
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw new DataBaseException(DATABASE_ERROR, e);
        }
    }

    /**
     * Сохраняет информацию о пользрователе в БД. Если пользователь содержит идентификатор,
     * то обновляет информацию об этом пользователе, если нет, то добавляет пользователя в БД
     * и присваивает еу уникальный идентификатор
     *
     * @param user пользователь, информацию о котором надо сохранить в БД
     * @return информацию о пользователе
     * @throws DataBaseException при возникновении ошибки при сохранении информации о пользователе
     */
    @Override
    public User save(User user) throws DataBaseException {
        String sql;
        if (user.getId() == null) {
            //insert
            sql = "INSERT INTO users (login, name, type, pass, block) VALUES ('" +
                    user.getLogin() + "', '" + user.getName() + "', '" +
                    user.getType() + "', '" + user.getPass() + "', '" + user.getBlock() + "')";
        }
        else {
            //update
            sql = "UPDATE users SET login = '" + user.getLogin() + "', name = '" +
                    user.getName() + "', type = '" + user.getType() + "', pass = '" +
                    user.getPass() + "', block = '" + user.getBlock() +
                    "' WHERE id = '" + user.getId() + "'";
        }
        try (Connection connection = connectionPool.getConnectionPool().getConnection();
             Statement statement = connection.createStatement()) {
            int count = statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            if (count == 1) {
                if (user.getId() == null) {
                    int id = 0;
                    ResultSet rs = statement.getGeneratedKeys();
                    if (rs.next()) {
                        id = rs.getInt(1);
                    }
                    user.setId(id);
                }
            }
            else {
                throw new DataBaseException(USER_CHANGE_ERROR);
            }
        } catch (MySQLIntegrityConstraintViolationException e){
            throw new DataBaseException(LOGIN_BISY,e);
        } catch (Exception e) {
            throw new DataBaseException(DATABASE_ERROR, e);
        }
        return user;
    }

    /**
     * Функция удаления информации о пользователе из БД
     *
     * @param user объект с информацией о пользователе, подлежащем удалению
     * @throws DataBaseException при возникновении ошибок при удалении
     */
    @Override
    public void delete(User user) throws DataBaseException {
        if (user.getId() == null) {
            String sql = "DELETE FROM users WHERE id = '" + user.getId() + "'";
            try (Connection connection = connectionPool.getConnectionPool().getConnection();
                 Statement statement = connection.createStatement()) {
                int count = statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
                if (count == 0) {
                    throw new DataBaseException(USER_NOT_FOUND);
                }
            }
            catch (SQLException | NamingException e) {
                e.printStackTrace();
                throw new DataBaseException(DATABASE_ERROR, e);
            }
        }
        else {
            throw  new DataBaseException(USER_IS_NULL);
        }
    }

    /**
     * Функция получения информации обо всех пользователях, зарегистрированных в системе4
     *
     * @return Возвращает список пользователей соответствующих всем записям в базе данных
     * @throws DataBaseException При ошибке во время работы с БД
     */
    @Override
    public List<User> getAll() throws DataBaseException {
        List<User> users = new ArrayList<>();
        String sql = "SELECT * FROM users";
        try (Connection connection = connectionPool.getConnectionPool().getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                User user = new User(new User.Builder().withId(rs.getInt("id"))
                        .withLogin(rs.getString("login")).withName(rs.getString("name"))
                        .withPass(rs.getString("pass")).withType(rs.getInt("type")).withBlock(rs.getInt("block")));
                users.add(user);
            }
            return users;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw new DataBaseException(DATABASE_ERROR, e);
        }
    }
}
