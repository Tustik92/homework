package ru.innopolis.course2.homework2.dao.mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.course2.homework2.dao.StudentLessonRelDao;
import ru.innopolis.course2.homework2.utils.ConnectionPool;
import ru.innopolis.course2.homework2.utils.DataBaseException;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static ru.innopolis.course2.homework2.utils.Constants.DATABASE_ERROR;

/**
 * Интерфейс для Dao для работы с записью на уроки, хранящимися в MySQL базе данных
 * содержит функционал получения списка идентификаторов пользователей, записанных на урок
 * и списка уроков, на которые записан ученик.
 *
 * @author Ruslan Tupikov
 */
public class MySQLStudentLessonRelDao implements StudentLessonRelDao {

    /**
     * Ссылка на пул соединений для получения соединений с базой данных
     */
    @Autowired
    private ConnectionPool connectionPool;

    /**
     * Получение списка идентификаторов пользователей, записанных на урок
     * @param lessonId идентификатор урока для получения пользователей
     * @return список идентификаторов пользователей
     * @throws DataBaseException привозникновении ошибки при работе с БД
     */
    @Override
    public List<Integer> getLessonStudents(Integer lessonId) throws DataBaseException {

        try (Connection connection = connectionPool.getConnectionPool().getConnection();
             Statement statement = connection.createStatement()) {
            List<Integer> students = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery("SELECT `student_id` FROM `students-lessons-rel` WHERE `lesson_id` = '" + lessonId + "'");
            while (resultSet.next()) {
                students.add(resultSet.getInt(1));
            }
            return students;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw new DataBaseException(DATABASE_ERROR, e);
        }
    }

    /**
     * Получение списка идентификаторов уроков, на которые записан ученик
     * @param student_id идентификатор ученика для получения списка уроков
     * @return список идентификаторов уроков
     * @throws DataBaseException привозникновении ошибки при работе с БД
     */
    @Override
    public List<Integer> getStudentLessons(Integer student_id) throws DataBaseException {
        return null;
    }
}
