package ru.innopolis.course2.homework2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.course2.homework2.dao.LessonDao;
import ru.innopolis.course2.homework2.dao.StudentLessonRelDao;
import ru.innopolis.course2.homework2.dao.UserDao;
import ru.innopolis.course2.homework2.entities.Lesson;
import ru.innopolis.course2.homework2.entities.User;
import ru.innopolis.course2.homework2.model.LessonModel;
import ru.innopolis.course2.homework2.model.UserModel;
import ru.innopolis.course2.homework2.utils.DataBaseException;
import ru.innopolis.course2.homework2.utils.OrikaMapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Андрей on 30.11.2016.
 */
@Component
public class LessonService {

    @Autowired
    LessonDao lessonDao;

    @Autowired
    UserDao userDao;

    @Autowired
    private OrikaMapper mapper;

    @Autowired
    StudentLessonRelDao studentLessonRelDao;

    public List<LessonModel> getTeacherLessons(int teacherId) throws DataBaseException {
        List<Lesson> lessons = lessonDao.getAll(teacherId);
        List<LessonModel> lessonModels = new ArrayList<>();
        for (Lesson lesson : lessons) {
            lessonModels.add(mapper.lessonToModel(lesson));
        }
        List<LessonModel> activeLessons = new ArrayList<>();
        for (LessonModel lesson : lessonModels) {
            if (lesson.getDate().before(new Date())){
                continue;
            }
            activeLessons.add(lesson);
        }
        return activeLessons;
    }

    public List<UserModel> getLessonStudents (int lessonId) throws DataBaseException {
        List<Integer> studentIDs = studentLessonRelDao.getLessonStudents(lessonId);
        List<UserModel> students = new ArrayList<>();
        for (int id: studentIDs) {
            User student = userDao.read(id);
            students.add(mapper.userToModel(student));
        }
        return students;
    }

    public LessonModel getLesson(Integer lessonId) throws DataBaseException {
        Lesson lesson = lessonDao.read(lessonId);
        return mapper.lessonToModel(lesson);
    }

    public void addLesson(int teacher, String topic, int day, int month, int year, int hour, int minutes ) throws DataBaseException {
        Date date = new Date();
        date.setYear(year);
        date.setMonth(month);
        date.setDate(day);
        date.setHours(hour);
        date.setMinutes(0);
        date.setSeconds(0);
        Lesson lesson = new Lesson()
                .setDate(date)
                .setTeacher(teacher)
                .setTopic(topic)
                .setMinutes(minutes);
        lessonDao.save(lesson);
    }
}
