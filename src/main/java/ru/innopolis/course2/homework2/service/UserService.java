package ru.innopolis.course2.homework2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.innopolis.course2.homework2.dao.UserDao;
import ru.innopolis.course2.homework2.entities.User;
import ru.innopolis.course2.homework2.model.UserModel;
import ru.innopolis.course2.homework2.utils.DataBaseException;
import ru.innopolis.course2.homework2.utils.OrikaMapper;

import javax.servlet.http.HttpSession;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static ru.innopolis.course2.homework2.utils.Constants.*;

/**
 *
 * @author Ruslan Tupikov
 */
@Component
public class UserService implements PasswordEncoder {

    /**
     * Dao для работы с информацией о пользователях.
     */
    @Autowired
    UserDao userDao;

    @Autowired
    private OrikaMapper mapper;

    /**
     * Осуществляет вход пользователя в систему
     * Если вход выполнен успешно, то сохраняет идентификатор пользователя в сессию.
     *
     * @param login логин пользователя из формы авторизации
     * @param pass пароль пользователя из формы авторизации
     * @return Ссылку на пользователя при успешном входе
     * @throws DataBaseException если вход выполнить не удалось или произошла ошибка при работе с БД
     */
    /*public User login (String login, String pass) throws DataBaseException {
        try {
            pass = salting(pass);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        User user = userDao.identify(login, pass);
        if (user != null) {
            session().setAttribute("user", user.getId());
            return user;
        }
        else {
            throw new DataBaseException(LOGIN_FAILED);
        }
    }*/

    /**
     * Функция получения указателя на пользователя по его уникальному идентификатору
     *
     * @param id уникальный идентификатор пользователя
     * @return возвращает ссылку на пользователя
     * @throws DataBaseException если нет пользователя с таким ID или произошла ошибка в БД
     */
    public UserModel getUser(int id) throws DataBaseException {
        User u = userDao.read(id);
        return mapper.userToModel(u);
    }
    public UserModel getUserByLogin(String login) throws DataBaseException {
        User u = userDao.read(login);
        return mapper.userToModel(u);
    }

    /**
     * Функция для шифрования пароля. Использует двойной md5 хэширование и соль
     * @param pass пароль для хэширования
     * @return зашифрованный пароль
     * @throws NoSuchAlgorithmException
     */
    public static String salting (String pass) throws NoSuchAlgorithmException {
        return md5(md5(pass) + SALT);
    }

    /**
     * Функция получения md5 хэше строки
     *
     * @param pass Строка для хэширования
     * @return Возвращает md5 хэш стоки
     * @throws NoSuchAlgorithmException
     */
    private static String md5 (String pass) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.reset();
        m.update(pass.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1,digest);
        return bigInt.toString(16);
    }

    /**
     * Метод для получения указателя на сессию.
     *
     * @return Указатель на текущую сессию
     */
    private HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true);
    }

    /**
     * Регистрация нового пользователя самим пользователем или администратором.
     * Если регистрация прошла успешно, и ее проводит сам пользователь,
     * то сохраняет идентификатор пользрователя в сессию.
     *
     * @param name Имя пользователя
     * @param login Логин пользователя
     * @param pass Пароль
     * @param pass2 Повтор пароля
     * @param type Класс пользователя
     * @throws DataBaseException Если регистрация не удалась
     */
    public void register(String name, String login, String pass, String pass2, Integer type) throws DataBaseException {

        if (name == null || name.isEmpty()) {
            throw new DataBaseException(NAME_IS_NULL);
        }
        if (login == null || login.length() < 3) {
            throw new DataBaseException(LOGIN_TOO_SHORT);
        }
        if (pass == null || pass.length() < 6) {
            throw new DataBaseException(PASSWORD_TOO_SHORT);
        }
        if (!pass.equals(pass2)){
            throw new DataBaseException(PASSWORDS_DOESNT_MATCH);
        }
        if (type == null){
            type = 1;
        }
        try {
            pass = salting(pass);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        User user = new User(new User.Builder()
                .withName(name)
                .withBlock(0)
                .withLogin(login)
                .withPass(pass)
                .withType(type));
        user = userDao.save(user);
        if (session().getAttribute("user") == null) {
            session().setAttribute("user", user.getId());
        }
    }

    /**
     * Функция завершения пользовательской сессии
     */
    public void logout() {
        session().invalidate();
    }

    /**
     * Получение коллекции, содежащей список всех пользователей, зарегистрированных в системе.
     * @return коллекцию объектов пользователей
     * @throws DataBaseException при ошибке чтения из БД
     */
    public List<UserModel> getUserList() throws DataBaseException {
        List<User> users = userDao.getAll();
        List<UserModel> userModels = new ArrayList<>();
        for (User user : users) {
            userModels.add(mapper.userToModel(user));
        }
        return userModels;
    }

    /**
     * Изменение статуса блокировки у пользователя с полученным идентификатором
     * Если пользован заблокирован - разблокирует его, и наоборот.
     *
     * @param id идентификатор пользователя для изменения блокировки
     * @throws DataBaseException при возникновении ошибок во время работы с базой
     */
    public void block(int id) throws DataBaseException {
        User user = userDao.read(id);
        if (user.getBlock() == 0){
            user.setBlock(1);
        } else {
            user.setBlock(0);
        }
        userDao.save(user);
    }

    /**
     * Возвращает информацию о текущем пользователе
     * Получает идентификатор пользователя из сессии, и берет информацию об этом пользователе
     * @return ссылку на объект пользователя
     * @throws DataBaseException при возникновении ошибок во время работы с базой
     */
    public UserModel getCurrentUser() throws DataBaseException {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails)principal).getUsername();
        return getUserByLogin(username);
    }

    /**
     * Обновляет информацию о пользователе. Изменяет имя пользователя и пароль,
     * если они удовлетворяют требованиям
     *
     * @param name Новое имя пользователя
     * @param pass Новый пароль
     * @param pass2 повтор нового пароля
     * @throws DataBaseException при возникновении ошибок во время работы с базой
     * или если имя пользователя или пароль не удовлетворяют требованиям
     */
    public void editCurrentUser(String name, String pass, String pass2) throws DataBaseException {
        if (name == null || name.isEmpty()) {
            throw new DataBaseException(NAME_IS_NULL);
        }
        if (pass == null || pass.length() < 6) {
            throw new DataBaseException(PASSWORD_TOO_SHORT);
        }
        if (!pass.equals(pass2)){
            throw new DataBaseException(PASSWORDS_DOESNT_MATCH);
        }
        UserModel user = getCurrentUser();
        user.setName(name);
        try {
            user.setPass(salting(pass));
            userDao.save(mapper.userToEntity(user));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new DataBaseException(DATABASE_ERROR);
        }
    }

    @Override
    public String encode(CharSequence rawPassword) {
        try {
            return salting((String) rawPassword);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return (String) rawPassword;
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        try {
            return encodedPassword.equals(salting((String)rawPassword));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return false;
    }
}
