package ru.innopolis.course2.homework2.utils;

import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;
import ru.innopolis.course2.homework2.entities.Lesson;
import ru.innopolis.course2.homework2.entities.User;
import ru.innopolis.course2.homework2.model.LessonModel;
import ru.innopolis.course2.homework2.model.UserModel;

/**
 * Created by Андрей on 09.12.2016.
 */
@Component
public class OrikaMapper {
    private MapperFactory userEntityMapperFactory = new DefaultMapperFactory.Builder().build();
    private MapperFactory userModelMapperFactory = new DefaultMapperFactory.Builder().build();
    private MapperFactory lessonEntityMapperFactory = new DefaultMapperFactory.Builder().build();
    private MapperFactory lessonModelMapperFactory = new DefaultMapperFactory.Builder().build();
    public OrikaMapper() {
        userEntityMapperFactory.classMap(User.class, UserModel.class)
                .byDefault()
                .register();
        userModelMapperFactory.classMap(UserModel.class,User.class)
                .byDefault()
                .register();
        lessonEntityMapperFactory.classMap(Lesson.class, LessonModel.class)
                .byDefault()
                .register();
        lessonModelMapperFactory.classMap(LessonModel.class,Lesson.class)
                .byDefault()
                .register();
    }

    public UserModel userToModel(User user) {
        MapperFacade mapper = userEntityMapperFactory.getMapperFacade();
        return mapper.map(user, UserModel.class);
    }
    public User userToEntity(UserModel user) {
        MapperFacade mapper = userModelMapperFactory.getMapperFacade();
        return mapper.map(user, User.class);
    }

    public LessonModel lessonToModel(Lesson lesson) {
        MapperFacade mapper = lessonEntityMapperFactory.getMapperFacade();
        return mapper.map(lesson, LessonModel.class);
    }
    public Lesson lessonToEntity(LessonModel lessonModel) {
        MapperFacade mapperFacade = lessonModelMapperFactory.getMapperFacade();
        return mapperFacade.map(lessonModel,Lesson.class);
    }
}
