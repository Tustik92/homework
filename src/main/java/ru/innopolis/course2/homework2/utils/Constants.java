package ru.innopolis.course2.homework2.utils;

/**
 * Created by Андрей on 20.11.2016.
 */
public class Constants {
    public static final String POOL_NAME = "java:comp/env/jdbc/connectionPool";

    /**
     * Описания ошибок и сообщений при работе с системой
     */
    public static final String PASSWORDS_DOESNT_MATCH = "Введенные пароли не совпадают!";
    public static final String PASSWORD_TOO_SHORT = "Длина пароля должна быть не меньше 6 символов!";
    public static final String LOGIN_TOO_SHORT = "Длина логина должна быть не меньше 3 символов!";
    public static final String NAME_IS_NULL = "Введите Ваше имя!";
    public static final String LOGIN_BISY = "Логин уже занят!";
    public static final String LOGIN_FAILED = "Не наден пользователь с соответствующим логином и паролем!";
    public static final String USER_CHANGE_ERROR = "Ошибка изменения пользователя!";
    public static final String USER_NOT_FOUND = "Пользователь не найден в БД.";
    public static final String USER_IS_NULL = "Нет пользователя для удаления!";
    public static final String USER_IS_BLOCKED = "Ваша учетная запись заблокирована!";
    public static final String DATABASE_ERROR = "Ошибка при работе с базой данных, попробуйте еще раз!";
    public static final String BLOCK_SUCCESS = "Блокировка пользователя изменена";
    public static final String LESSON_NOT_FOUND = "Урок не найден";
    public static final String ACCESS_ERROR = "Урок не найден";

    /**
     * Соль для пароля
     */
    public static final String SALT = "SALTSALT321S";
}
