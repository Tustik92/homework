package ru.innopolis.course2.homework2.utils;

public class DataBaseException extends Exception {


    public DataBaseException(String message) {
        super(message);
    }

    public DataBaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataBaseException(Throwable cause) {
        super(cause);
    }

}
