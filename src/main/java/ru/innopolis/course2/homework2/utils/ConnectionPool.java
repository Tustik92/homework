package ru.innopolis.course2.homework2.utils;

import org.springframework.stereotype.Component;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static ru.innopolis.course2.homework2.utils.Constants.POOL_NAME;

/**
 * Пул соединений. Использует пул, встроенный в контекст контейнера сервлетов.
 * @author Ruslan Tupikov
 */
@Component
public class ConnectionPool {

    /**
     * Статичный метод для полцчения ссылки на DataSource
     *
     * @return Ссылку на DataSource для получения соединений
     * @throws NamingException при отсутствии пула в контексте
     */
    public static DataSource getConnectionPool() throws NamingException {
        InitialContext initContext= new InitialContext();
        return (DataSource) initContext.lookup(POOL_NAME);
    }

}
