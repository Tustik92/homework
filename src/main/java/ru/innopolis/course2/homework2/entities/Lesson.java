package ru.innopolis.course2.homework2.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * POJO объект для хранения информации об уроке
 *
 * @author Ruslan Tupikov
 */

@Entity
@Table(name = "lessons")
public class Lesson {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id",unique=true, nullable = false)
    private Integer id;

    @Column(name = "teacher")
    private int teacher;

    @Column (name = "topic")
    private String topic;

    @Column (name = "date")
    private Date date;

    @Column (name = "minutes")
    private int minutes;

    public int getId() {
        return id;
    }

    public Lesson setId(int id) {
        this.id = id;
        return this;
    }

    public int getTeacher() {
        return teacher;
    }

    public Lesson setTeacher(int teacher) {
        this.teacher = teacher;
        return this;
    }

    public String getTopic() {
        return topic;
    }

    public Lesson setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public Lesson setDate(Date date) {
        this.date = date;
        return this;
    }

    public int getMinutes() {
        return minutes;
    }

    public Lesson setMinutes(int minutes) {
        this.minutes = minutes;
        return this;
    }
}
