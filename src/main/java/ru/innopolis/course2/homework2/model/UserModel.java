package ru.innopolis.course2.homework2.model;


/**
 * Created by Андрей on 09.12.2016.
 */
public class UserModel implements User {
    /**
     * Имя пользователя
     */
    private String name;
    /**
     * Login пользователя
     */
    private String login;
    /**
     * Информация о статусе пользователя
     * (1 - ученик, 2 - преподаватель, 3 - администратор)
     */
    private int type;
    /**
     * Уникальный идентификатор пользователя в системе
     */
    private Integer id;
    /**
     * Пароль пользователя. Хранится в шифрованном виде.
     */
    private String pass;
    /**
     * Информация о блокировке пользователя.
     * (1 - заблокирован, 0 - нет)
     */
    private int block;

    public UserModel(Builder builder) {
        this.name = builder.name;
        this.login = builder.login;
        this.type = builder.type;
        this.id = builder.id;
        this.pass = builder.pass;
        this.block = builder.block;
    }

    public UserModel(){}

    public int getType() {
        return type;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public int getBlock() {
        return block;
    }

    public String getPass() {
        return pass;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setBlock(int block) {
        this.block = block;
    }

    /**
     * Builder для пользователя содержит значения по умолчанию.
     */
    public static class Builder {
        private String name;
        private String login;
        private int type = 1;
        private Integer id = null;
        private String pass;
        private int block = 0;

        public Builder withId(Integer id) {
            this.id = id;
            return this;
        }
        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withLogin(String login) {
            this.login = login;
            return this;
        }

        public Builder withType(int type) {
            this.type = type;
            return this;
        }

        public Builder withPass(String pass) {
            this.pass = pass;
            return this;
        }

        public Builder withBlock(int block) {
            this.block = block;
            return this;
        }
    }
}
