package ru.innopolis.course2.homework2.model;

import java.util.Date;

/**
 * POJO объект для хранения информации об уроке
 *
 * @author Ruslan Tupikov
 */
public class LessonModel implements Lesson{
    private Integer id;
    private int teacher;
    private String topic;
    private Date date;
    private int minutes;

    public Integer getId() {
        return id;
    }

    public LessonModel setId(Integer id) {
        this.id = id;
        return this;
    }

    public int getTeacher() {
        return teacher;
    }

    public LessonModel setTeacher(int teacher) {
        this.teacher = teacher;
        return this;
    }

    public String getTopic() {
        return topic;
    }

    public LessonModel setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public LessonModel setDate(Date date) {
        this.date = date;
        return this;
    }

    public int getMinutes() {
        return minutes;
    }

    public LessonModel setMinutes(int minutes) {
        this.minutes = minutes;
        return this;
    }
}
