package ru.innopolis.course2.homework2.model;

/**
 * Created by Андрей on 09.12.2016.
 */
public interface User {

    int getType();

    Integer getId();

    String getName();

    String getLogin();

    int getBlock();

    void setId(Integer id);

    void setName(String name);

    void setLogin(String login);

    void setType(int type);

    void setBlock(int block);

}
