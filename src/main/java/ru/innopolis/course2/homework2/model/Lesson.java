package ru.innopolis.course2.homework2.model;

import java.util.Date;

/**
 * Created by Андрей on 09.12.2016.
 */
public interface Lesson {
    Integer getId();

    Lesson setId(Integer id);

    int getTeacher();

    Lesson setTeacher(int teacher);

    String getTopic();

    Lesson setTopic(String topic);

    Date getDate();

    Lesson setDate(Date date);

    int getMinutes();

    Lesson setMinutes(int minutes);
}
