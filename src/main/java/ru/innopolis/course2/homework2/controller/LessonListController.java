package ru.innopolis.course2.homework2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.course2.homework2.entities.Lesson;
import ru.innopolis.course2.homework2.entities.User;
import ru.innopolis.course2.homework2.model.LessonModel;
import ru.innopolis.course2.homework2.model.UserModel;
import ru.innopolis.course2.homework2.service.LessonService;
import ru.innopolis.course2.homework2.service.UserService;
import ru.innopolis.course2.homework2.utils.DataBaseException;

import java.util.List;

import static ru.innopolis.course2.homework2.utils.Constants.*;

/**
 * Created by Андрей on 30.11.2016.
 */
@Controller
public class LessonListController {

    Logger logger = LoggerFactory.getLogger(LessonListController.class);

    @Autowired
    LessonService lessonService;

    @Autowired
    UserService userService;

    @RequestMapping("/cabinet/teacher/lessons")
    public String getLessons(Model model) {
        try {
            UserModel teacher = userService.getCurrentUser();
            List<LessonModel> lessonList = lessonService.getTeacherLessons(teacher.getId());
            model.addAttribute("lessonList", lessonList);
        } catch (DataBaseException e) {
            logger.error("Error: ", e);
            model.addAttribute("error", e.getMessage());
        }
        return "lessonList";
    }

    @RequestMapping("/cabinet/teacher/lesson/{lessonId}")
    public String getLessons(@PathVariable ("lessonId") Integer lessonId,
                             Model model) {

        try {
            LessonModel lesson = lessonService.getLesson(lessonId);
            model.addAttribute("lesson",lesson);
            UserModel teacher = userService.getCurrentUser();
            if (teacher.getId().equals(lesson.getTeacher())) {
                List<UserModel> students = lessonService.getLessonStudents(lessonId);
                model.addAttribute("students", students);
            }
            else {
                throw new DataBaseException(ACCESS_ERROR);
            }
        } catch (DataBaseException e) {
            model.addAttribute("error", e.getMessage());
            logger.error("Error: ", e);
        }

        return "lessonInfo";
    }

    @RequestMapping ("/cabinet/teacher/add")
    public String showAddLesson() {
        return "addLesson";
    }

    @RequestMapping (value = "/cabinet/teacher/add", method = RequestMethod.POST)
    public String addLesson(@RequestParam ("topic") String topic,
                            @RequestParam ("day") Integer day,
                            @RequestParam ("month") Integer month,
                            @RequestParam ("year") Integer year,
                            @RequestParam ("hour") Integer hour,
                            @RequestParam ("minutes") Integer minutes,
                            Model model) {
        try {
            UserModel teacher = userService.getCurrentUser();
            lessonService.addLesson(teacher.getId(),topic,day,month,year,hour,minutes);
        } catch (DataBaseException e) {
            model.addAttribute("error", e.getMessage());
            logger.error("Error: ", e);
            return "addLesson";
        }
        return "redirect:/cabinet/teacher/lessons";
    }
}
