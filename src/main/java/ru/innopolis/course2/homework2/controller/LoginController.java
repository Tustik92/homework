package ru.innopolis.course2.homework2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.course2.homework2.entities.User;
import ru.innopolis.course2.homework2.service.UserService;
import ru.innopolis.course2.homework2.utils.DataBaseException;

/**
 * Контроллер формы авторизации пользователей
 * @author Ruslan Tupikov
 */
@Controller
public class LoginController {

    /**
     * UserService для работы с пользователями
     */
    @Autowired
    UserService userService;


    Logger logger = LoggerFactory.getLogger(LoginController.class);

    /**
     * Обработка данных, полученных из формы логирования.
     *
     * @param login логин введенный в форму авторизации
     * @param pass пароль введенный в форму авторизации
     * @param model модель для передачи в View. Содержит информацию о пользователе при успешном входе
     *              или информацию об ошибке, при возникновении ошибок авторизации
     * @return При успешном входе отправляет пользователя в личный кабинет,
     *         при ошибке авторизации подгружает шаблон формы авторизации
     */
    /*@RequestMapping (value = "/auth", method = RequestMethod.POST)
    public String handle(@RequestParam("login") String login, @RequestParam("password") String pass, Model model){
        try {
            User user = userService.login(login, pass);
            model.addAttribute("user", user);
            return "redirect:/cabinet";
        } catch (DataBaseException e) {
            model.addAttribute("error", e.getMessage());
            logger.error("Error: ", e);
            return "index";
        }
    }*/

    /**
     * Обработчик главной страницы
     *
     * @return подгружает шаблон формы авторизации.
     */
    @RequestMapping ("/auth")
    public String main() {
        return "index";
    }
}
