package ru.innopolis.course2.homework2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import ru.innopolis.course2.homework2.entities.User;
import ru.innopolis.course2.homework2.service.UserService;
import ru.innopolis.course2.homework2.utils.DataBaseException;

/**
 * Контроллер личного кабинета пользователей
 * @author Ruslan Tupikov
 */
@Controller
public class CabinetController {

    private static Logger logger = LoggerFactory.getLogger(CabinetController.class);

    /**
     * UserService для работы с пользователями
     */
    @Autowired
    UserService userService;

    /**
     * Отображение личного кабинета пользователя.
     *
     * @param model модель для передачи во View, содержит информацию о пользователе
     * @return подгружает шаблон личного кабинета
     */
    @RequestMapping ("/cabinet")
    public String cabinet(Model model) {
            try {
                model.addAttribute("user", userService.getCurrentUser());
                return "cabinet";
            } catch (DataBaseException e) {
                logger.error("Error: ", e);
                return "redirect:/";
            }
    }

    /**
     * Контроллер вывода формы редактирования профиля
     *
     * @param model возвращает информацию о текущем пользователе
     * @return подгружает форму редактирования профиля
     */
    @RequestMapping("/cabinet/edit")
    public String editProfile (Model model) {
        try {
            model.addAttribute("user", userService.getCurrentUser());
            return "profileEdit";
        } catch (DataBaseException e) {
            logger.error("Error: ", e);
            return "redirect:/cabinet";
        }
    }

    /**
     * Обработчик формы изменения профиля пользователя
     *
     * @param name Новое имя пользователя
     * @param pass Новый пароль
     * @param pass2 повтор нового пароля
     * @param model Сохраняет информацию об ошибке при возникновении исключения
     * @return отправляет в кабинет при успешном изменении профиля, или возвращает на страницу с формой редактирования
     */
    @RequestMapping(value = "/cabinet/edit", method = RequestMethod.POST)
    public String saveProfile (@RequestParam ("name") String name,
                               @RequestParam ("password") String pass,
                               @RequestParam ("password2") String pass2,
                               Model model) {
        try {
            userService.editCurrentUser(name, pass, pass2);
            return "redirect:/cabinet";
        } catch (DataBaseException e) {
            logger.error("Error: ", e);
            model.addAttribute("error", e.getMessage());
            return "redirect:/cabinet/edit";
        }
    }

}
