package ru.innopolis.course2.homework2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.innopolis.course2.homework2.entities.User;
import ru.innopolis.course2.homework2.service.UserService;
import ru.innopolis.course2.homework2.utils.DataBaseException;

import static ru.innopolis.course2.homework2.utils.Constants.*;

/**
 * Контроллер списка пользователей для администратора
 * @author Ruslan Tupikov
 */
@Controller
public class UserAdminController {

    /**
     * UserService для работы с пользователями
     */
    @Autowired
    UserService userService;

    Logger logger = LoggerFactory.getLogger(UserAdminController.class);

    /**
     * Получение списка всех зарегистрированных пользователей через UserService и вызов шаблона для вывода
     * @param model хранит в себе коллекцию пользователей при удачном получении списка
     *              пользователей или информацию об ошибке при возникновении ошибки
     * @return подгружает шаблон для вывода информации о пользователях
     */
    @RequestMapping("/cabinet/admin/list")
    String adminUserList (Model model) {
        try{
            model.addAttribute("userList", userService.getUserList());
        } catch (DataBaseException e) {
            logger.error("Error: ", e);
            model.addAttribute("error", e.getMessage());
        }
        return "userList";
    }

    /**
     * Изменяет статус блокирования у пользователя, чей идентификатор был передан GET-параметром
     *
     * @param id идентификатор пользователя, которому будет изменен статус блокирования
     * @param model хранит информацию об успехе операции или ошибке
     * @return перенаправляет на страницу со списком пользователей
     */
    @RequestMapping("/cabinet/admin/block")
    String adminUserBlock (@RequestParam ("user") Integer id, Model model) {
        try{
            userService.block(id);
            model.addAttribute("error", BLOCK_SUCCESS);
        } catch (DataBaseException e) {
            logger.error("Error: ", e);
            model.addAttribute("error", e.getMessage());
        }
        return "redirect:/cabinet/admin/list";
    }
}
