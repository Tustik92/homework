package ru.innopolis.course2.homework2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.innopolis.course2.homework2.service.UserService;
import ru.innopolis.course2.homework2.utils.DataBaseException;

/**
 * Контроллер формы регистрации пользователей
 * @author Ruslan Tupikov
 */
@Controller
public class RegistrationController {

    /**
     * UserService для работы с пользователями
     */
    @Autowired
    UserService userService;

    Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    /**
     * Обработчик URL формы регистрации
     * @return подгружает шаблон формы регистрации
     */
    @RequestMapping ("/register")
    String registrationForm () {
        return "register";
    }

    /**
     * Обработчик URL формы регистрации для администратора проекта
     * @return подгружает шаблон формы регистрации
     */
    @RequestMapping ("/cabinet/admin/register")
    String adminRegistrationForm () {
        return "register";
    }

    /**
     * Обработчик данных, полученных с формы регистрации
     * @param name имя пользователя, полученное из формы
     * @param login логин пользователя
     * @param pass пароль пользователя
     * @param pass2 повторение пароля
     * @param model в случае ошибки при регистрации содержит информацию об ошибке
     * @return При успешной регистрации перенаправляет в личный кабинет, при ошибке возвращает информацию об ошибке.
     */
    @RequestMapping (value = "/register", method = RequestMethod.POST)
    String registration (@RequestParam ("name") String name,
                         @RequestParam ("login") String login,
                         @RequestParam ("password") String pass,
                         @RequestParam ("password2") String pass2,
                         Model model) {
        try {
            userService.register(name, login, pass, pass2, null);
            return "redirect:/cabinet";
        } catch (DataBaseException e) {
            logger.error("Error: ", e);
            model.addAttribute("error", e.getMessage());
            return "register";
        }
    }

    /**
     * Обработчик данных, полученных с формы регистрации
     * @param name имя пользователя, полученное из формы
     * @param login логин пользователя
     * @param pass пароль пользователя
     * @param pass2 повторение пароля
     * @param model в случае ошибки при регистрации содержит информацию об ошибке
     * @return При успешной регистрации перенаправляет в личный кабинет, при ошибке возвращает информацию об ошибке.
     */
    @RequestMapping (value = "/cabinet/admin/register", method = RequestMethod.POST)
    String adminRegistration (@RequestParam ("name") String name,
                         @RequestParam ("login") String login,
                         @RequestParam ("password") String pass,
                         @RequestParam ("password2") String pass2,
                         @RequestParam ("type") Integer type,
                         Model model) {
        try {
            userService.register(name, login, pass, pass2, type);
            return "redirect:/cabinet/admin/list";
        } catch (DataBaseException e) {
            logger.error("Error: ", e);
            model.addAttribute("error", e.getMessage());
            return "register";
        }
    }
}
