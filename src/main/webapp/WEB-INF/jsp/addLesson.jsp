<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 18.11.2016
  Time: 16:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Создание урока - Обучатор 3000</title>
  </head>
  <body>
    <h1 style="text-align: center">Создание урока</h1>
    <form method="post" action="">
      <table style="width: 300px; margin: 0 auto;">
        <tr>
          <td colspan="2">
            <% if (request.getAttribute("error") != null) { %>
              <p style="text-align: center; color: red"><%= request.getAttribute("error") %></p>
            <% } %>
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="topic">Тема: </label>
          </td>
          <td>
            <input name="topic" id="topic" />
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="day">Число: </label>
          </td>
          <td>
            <select id="day" name="day">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">23</option>
              <option value="23">23</option>
              <option value="24">24</option>
              <option value="25">25</option>
              <option value="26">26</option>
              <option value="27">27</option>
              <option value="28">28</option>
              <option value="29">29</option>
              <option value="30">30</option>
              <option value="31">31</option>
            </select>
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="month">Месяц: </label>
          </td>
          <td>
            <select name="month" id="month">
              <option value="0">Январь</option>
              <option value="1">Февраль</option>
              <option value="2">Март</option>
              <option value="3">Апрель</option>
              <option value="4">Май</option>
              <option value="5">Июнь</option>
              <option value="6">Июль</option>
              <option value="7">Август</option>
              <option value="8">Сентябрь</option>
              <option value="9">Октябрь</option>
              <option value="10">Ноябрь</option>
              <option value="11">Декабрь</option>
            </select>
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="year">Год: </label>
          </td>
          <td>
            <select name="year" id="year">
              <option value="116">2016</option>
              <option value="117">2017</option>
            </select>
          </td>
        </tr>

        <tr>
          <td style="width: 100px">
            <label for="hour">Час: </label>
          </td>
          <td>
            <select id="hour" name="hour">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">23</option>
              <option value="23">23</option>
            </select>
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="minutes">Продолжительность (мин): </label>
          </td>
          <td>
            <input name="minutes" id="minutes" />
          </td>
        </tr>

        <tr>
          <td colspan="2">
            <input style="width: 300px; margin: 20px auto; display: block;" type="submit" value="Создать урок">
          </td>
        </tr>
          <tr>
            <td colspan="2">
              <p style="text-align: center">
                  <a href="/cabinet">Вернуться в кабинет</a>
              </p>
            </td>
          </tr>
      </table>
    </form>
  </body>
</html>
