<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 18.11.2016
  Time: 16:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
  <head>
    <title>Обучатор 3000</title>
  </head>
  <body>
  <h1 style="text-align: center">Вход в систему!</h1>
    <form action="<c:url value='/login' />" method="post">
      <table style="width: 300px; margin: 0 auto;">
        <tr>
          <td colspan="2">
            <p style="text-align: center; color: red">${error}</p>
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="login">Логин: </label>
          </td>
          <td>
            <input style="width: 200px" id="login" name="login" />
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="password">Пароль: </label>
          </td>
          <td>
            <input style="width: 200px" type="password" name="password" id="password">
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <input style="width: 300px; margin: 20px auto; display: block;" type="submit" value="Войти">
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <p style="text-align: center">
              <a href="/register">Регистрация</a>
            </p>
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>
