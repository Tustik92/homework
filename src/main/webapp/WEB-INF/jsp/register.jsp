<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 18.11.2016
  Time: 16:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Регистрация - Обучатор 3000</title>
  </head>
  <body>
    <h1 style="text-align: center">Регистрация</h1>
    <form method="post">
      <table style="width: 300px; margin: 0 auto;">
        <tr>
          <td colspan="2">
            <% if (request.getAttribute("error") != null) { %>
              <p style="text-align: center; color: red"><%= request.getAttribute("error") %></p>
            <% } %>
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="name">Имя: </label>
          </td>
          <td>
            <input style="width: 200px" id="name" name="name" />
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="login">Логин: </label>
          </td>
          <td>
            <input style="width: 200px" id="login" name="login" />
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="password">Пароль: </label>
          </td>
          <td>
            <input style="width: 200px" type="password" name="password" id="password">
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="password2">Повторите пароль: </label>
          </td>
          <td>
            <input style="width: 200px" type="password" name="password2" id="password2">
          </td>
        </tr>
        <%
          if (session.getAttribute("user") != null) { %>
            <tr>
              <td style="width: 100px">
                <label>Тип пользователя: </label>
              </td>
              <td>
                <input type="radio" name="type" checked value="1" /> Ученик<br>
                <input type="radio" name="type" value="2" /> Преподаватель<br>
                <input type="radio" name="type" value="3" /> Администратор
              </td>
            </tr>
        <% } %>
        <tr>
          <td colspan="2">
            <input style="width: 300px; margin: 20px auto; display: block;" type="submit" value="Зарегистрироваться">
          </td>
        </tr>
          <tr>
            <td colspan="2">
              <p style="text-align: center">
                <% if (session.getAttribute("user") == null){ %>
                  <a href="/">Авторизация</a>
                <% } else {%>
                  <a href="/cabinet">Вернуться в кабинет</a>
                <%}%>
              </p>
            </td>
          </tr>
      </table>
    </form>
  </body>
</html>
