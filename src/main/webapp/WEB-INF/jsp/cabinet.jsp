<%@ page import="ru.innopolis.course2.homework2.entities.User" %>
<%@ page import="ru.innopolis.course2.homework2.model.UserModel" %><%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 18.11.2016
  Time: 16:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Личный кабинет - Обучатор 3000</title>
  </head>
  <body>
    <h1 style="text-align: center">Кабинет</h1>
    <%
      UserModel u = (UserModel) request.getAttribute("user");
      int type = u.getType();
    %>
  <p>Здравствуйте, <%=u.getName()%>! Чем займемся сегодня?</p>
    <% if (type == 2) {%>
      <p><a href="/cabinet/teacher/lessons">Посмотреть свои уроки</a></p>
      <p><a href="/cabinet/teacher/add">Создать урок</a></p>
    <% }%>
    <% if (type == 3) { %>
      <p><a href="/cabinet/admin/register">Добавить пользователя</a></p>
      <p><a href="/cabinet/admin/list">Список пользователей</a></p>
    <% } %>
    <p><a href="/cabinet/edit">Редактировать профиль</a></p>

  <p><a href="/logout">Выйти</a></p>
  </body>
</html>
