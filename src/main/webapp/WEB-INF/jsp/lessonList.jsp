<%@ page import="java.text.SimpleDateFormat" %><%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 29.11.2016
  Time: 0:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
  <title>Список Ваших уроков</title>
</head>
<body>
<h1 style="text-align: center">Список Ваших уроков</h1>
<% if (request.getParameter("error") != null) {%>
<h3 style="color: #ff3e54;text-align: center"><%=request.getParameter("error")%></h3>
<%} else {%>
<table style="width: 80%; margin: 20px auto;" border="1">
    <tr>
        <td>Тема</td>
        <td>Время начала</td>
        <td>Продолжительность, мин</td>
        <td></td>
    </tr>
    <c:forEach items="${lessonList}" var="lesson">
    <tr>
        <td><c:out value="${lesson.getTopic()}" /></td>
        <td><fmt:formatDate type="both" dateStyle="long" value="${lesson.getDate()}" /></td>
        <td><c:out value="${lesson.getMinutes()}" /></td>
        <td><a href="/cabinet/teacher/lesson/<c:out value="${lesson.getId()}" />">Список учеников</a></td>
    </tr>
    </c:forEach>
</table>
<%}%>
<a style="display: block; margin: 10px auto; text-align: center" href="/cabinet">Вернуться в кабинет</a>
</body>
</html>
