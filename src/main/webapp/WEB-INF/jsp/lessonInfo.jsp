<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 29.11.2016
  Time: 0:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
  <title>Список пользователей</title>
</head>
<body>
<h1 style="text-align: center">Информация об уроке</h1>
<% if (request.getParameter("error") != null) {%>
<h3 style="color: #ff3e54;text-align: center"><%=request.getParameter("error")%></h3>
<%}%>
<p><b>Тема урока: ${lesson.getTopic()}</b></p>
<p><b>Время проведения урока: <fmt:formatDate type="both" dateStyle="long" value="${lesson.getDate()}" /></b></p>

<p><b>Записавшиеся ученики:</b></p>
    <ul>
    <c:forEach items="${students}" var="student">
        <li><c:out value="${student.getName()}" /></li>
    </c:forEach>
    </ul>
<a href="/cabinet/teacher/lessons">Назад</a>
</body>
</html>
