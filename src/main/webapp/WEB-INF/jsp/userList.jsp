<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 29.11.2016
  Time: 0:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <title>Список пользователей</title>
</head>
<body>
<h1 style="text-align: center">Список пользователей</h1>
<% if (request.getParameter("error") != null) {%>
<h3 style="color: #ff3e54;text-align: center"><%=request.getParameter("error")%></h3>
<%}%>
<table style="width: 80%; margin: 20px auto;" border="1">
    <tr>
        <td>ID</td>
        <td>Имя</td>
        <td>Login</td>
        <td>Статус</td>
        <td>Блокировка</td>
    </tr>
    <c:forEach items="${userList}" var="user">
    <tr>
        <td><c:out value="${user.getId()}" /></td>
        <td><c:out value="${user.getName()}" /></td>
        <td><c:out value="${user.getLogin()}" /></td>
        <td><c:out value="${user.getType()}" /></td>
        <td><a href="/cabinet/admin/block/?user=<c:out value="${user.getId()}" />"><c:out value="${user.getBlock() == 0 ? 'Заблокировать': 'Разблокировать'}"/></a></td>
    </tr>
    </c:forEach>
</table>
<a style="display: block; margin: 10px auto; text-align: center" href="/cabinet">Вернуться в кабинет</a>
</body>
</html>
