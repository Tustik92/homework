<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 18.11.2016
  Time: 16:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Редактирование профиля - Обучатор 3000</title>
  </head>
  <body>
    <h1 style="text-align: center">Редактирование профиля</h1>
    <form method="post" action="">
      <table style="width: 300px; margin: 0 auto;">
        <tr>
          <td colspan="2">
            <% if (request.getParameter("error") != null) { %>
              <p style="text-align: center; color: red"><%= request.getParameter("error") %></p>
            <% } %>
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="name">Имя: </label>
          </td>
          <td>
            <input style="width: 200px" id="name" name="name" value="${user.getName()}" />
          </td>
        </tr>
          <td style="width: 100px">
            <label for="password">Новый пароль: </label>
          </td>
          <td>
            <input style="width: 200px" type="password" name="password" id="password">
          </td>
        </tr>
        <tr>
          <td style="width: 100px">
            <label for="password2">Повторите пароль: </label>
          </td>
          <td>
            <input style="width: 200px" type="password" name="password2" id="password2">
          </td>
        </tr>
          <td colspan="2">
            <input style="width: 300px; margin: 20px auto; display: block;" type="submit" value="Сохранить изменения">
          </td>
        </tr>
          <tr>
            <td colspan="2">
              <p style="text-align: center">
                  <a href="/cabinet">Вернуться в кабинет</a>
              </p>
            </td>
          </tr>
      </table>
    </form>
  </body>
</html>
